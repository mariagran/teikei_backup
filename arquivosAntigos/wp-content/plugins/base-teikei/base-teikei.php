<?php
/**
 * Plugin Name: Base Teikei
 * Description: Controle base do tema Teikei.
 * Version: 0.1
 * Author: Hudson Caronilo
 * Author URI: 
 * Licence: GPL2
 */

	function baseTeikei () {

		//TIPOS DE CONTEÚDO
		conteudosTeikei();

		//TAXONOMIA
		taxonomiaTeikei();

		//META BOXES
		metaboxesTeikei();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
		function conteudosTeikei (){

			// TIPOS DE DESTQUE
			tipoDestaque();

			// SERVIÇOS
			tipoServico();

			//DEPOIMENTOS
			tipoDepoimento();

			//PARCEIROS
			tipoParceiro();

			//PRODUTOS
			tipoProduto();

			// EQUIPE
			tipoEquipe();

			// TREINAMENTOS
			tipoTreinamentos();



			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){

				switch (get_current_screen()->post_type) {

					case 'equipe':
						$titulo = 'Nome do integrante';
					break;

					default:
					break;
				}
			    return $titulo;
			}

		}
	
	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE HABILIDADES/FERRAMENTAS
		function tipoHabilidades() {

			$rotulosHabilidades = array(
									'name'               => 'Habilidade e Ferramenta',
									'singular_name'      => 'habilidade e ferramenta',
									'menu_name'          => 'Habilidades e Ferramentas',
									'name_admin_bar'     => 'Habilidades e Ferramentas',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova Habilidade ou Ferramenta',
									'new_item'           => 'Nova habilidades ou ferramentas',
									'edit_item'          => 'Editar habilidades ou ferramentas',
									'view_item'          => 'Ver habilidades ou ferramentas',
									'all_items'          => 'Todos as habilidades e ferramentas',
									'search_items'       => 'Buscar habilidades ou ferramentas',
									'parent_item_colon'  => 'Das habilidades e ferramentas',
									'not_found'          => 'Nenhuma habilidades ou ferramentas cadastrado.',
									'not_found_in_trash' => 'Nenhuma habilidades ou ferramentas na lixeira.'
								);

			$argsHabilidades 	= array(
									'labels'             => $rotulosHabilidades,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-hammer',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'habilidades-ferramentas' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('habilidades', $argsHabilidades);

		}

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoServico() {

			$rotulosServico = array(
									'name'               => 'Serviços',
									'singular_name'      => 'Serviço',
									'menu_name'          => 'Serviços',
									'name_admin_bar'     => 'Serviços',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo serviço',
									'new_item'           => 'Novo serviço',
									'edit_item'          => 'Editar serviço',
									'view_item'          => 'Ver serviço',
									'all_items'          => 'Todos os serviços',
									'search_items'       => 'Buscar serviços',
									'parent_item_colon'  => 'Dos serviços',
									'not_found'          => 'Nenhum serviço cadastrado.',
									'not_found_in_trash' => 'Nenhum serviço na lixeira.'
								);

			$argsServico 	= array(
									'labels'             => $rotulosServico,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-tools',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'servicos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('servico', $argsServico);

		}

		// CUSTOM POST TYPE DEPOIMENTO
		function tipoDepoimento() {

			$rotulosDepoimento = array(
									'name'               => 'Depoimentos',
									'singular_name'      => 'Depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'Depoimentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos os depoimentos',
									'search_items'       => 'Buscar depoimentos',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimento 	= array(
									'labels'             => $rotulosDepoimento,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-heart',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimento' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimento', $argsDepoimento);

		}

		// CUSTOM POST TYPE PARCEIRO
		function tipoParceiro() {

			$rotulosParceiro = array(
									'name'               => 'Parceiros',
									'singular_name'      => 'parceiro',
									'menu_name'          => 'Parceiros',
									'name_admin_bar'     => 'Parceiros',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo parceiro',
									'new_item'           => 'Novo parceiro',
									'edit_item'          => 'Editar parceiro',
									'view_item'          => 'Ver parceiro',
									'all_items'          => 'Todos os parceiros',
									'search_items'       => 'Buscar parceiros',
									'parent_item_colon'  => 'Dos parceiros',
									'not_found'          => 'Nenhum parceiro cadastrado.',
									'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
								);

			$argsParceiro 	= array(
									'labels'             => $rotulosParceiro,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-universal-access',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'parceiros' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('parceiros', $argsParceiro);

		}

		// CUSTOM POST TYPE PRODUtOS
		function tipoProduto() {
			$rotulosProduto = array(
									'name'               => 'Produto',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Produtos',
									'name_admin_bar'     => 'Produtos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo produto',
									'new_item'           => 'Novo produtos',
									'edit_item'          => 'Editar produto',
									'view_item'          => 'Ver produtos',
									'all_items'          => 'Todos os produtos',
									'search_items'       => 'Buscar produtos',
									'parent_item_colon'  => 'Dos produtos',
									'not_found'          => 'Nenhum produto cadastrado.',
									'not_found_in_trash' => 'Nenhum produto na lixeira.'
								);
			$argsProduto 	= array(

									'labels'             => $rotulosProduto,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-products',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'produto' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('produto', $argsProduto);

		}

		// CUSTOM POST TYPE COLABORADOR
		function tipoEquipe() {

			$rotulosEquipe = array(
									'name'               => 'especialista',
									'singular_name'      => 'especialista',
									'menu_name'          => 'Especialista',
									'name_admin_bar'     => 'especialista',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo membro',
									'new_item'           => 'Novo membro',
									'edit_item'          => 'Editar membro',
									'view_item'          => 'Ver membro',
									'all_items'          => 'Todos os membros',
									'search_items'       => 'Buscar membros',
									'parent_item_colon'  => 'Dos membros',
									'not_found'          => 'Nenhum membro cadastrado.',
									'not_found_in_trash' => 'Nenhum membro na lixeira.'
								);

			$argsEquipe 	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-nametag',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);

		}

		// CUSTOM POST TYPE COLABORADOR
		function tipoTreinamentos() {

			$rotulosTreinamentos = array(
									'name'               => 'treinamento',
									'singular_name'      => 'treinamento',
									'menu_name'          => 'Treinamentos',
									'name_admin_bar'     => 'treinamento',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo treinamento',
									'new_item'           => 'Novo treinamento',
									'edit_item'          => 'Editar treinamento',
									'view_item'          => 'Ver treinamento',
									'all_items'          => 'Todos os treinamentos',
									'search_items'       => 'Buscar treinamento',
									'parent_item_colon'  => 'Dos treinamentos',
									'not_found'          => 'Nenhum treinamento cadastrado.',
									'not_found_in_trash' => 'Nenhum treinamento na lixeira.'
								);

			$argsTreinamentos 	= array(
									'labels'             => $rotulosTreinamentos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-hammer',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'treinamentos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('treinamentos', $argsTreinamentos);

		}
	/****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesTeikei(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'Teikei_';

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxDestaque',
					'title'			=> 'Detalhes do destaque',
					'pages' 		=> array('destaque'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Link do destaque: ',
							'id'    => "{$prefix}destaque_link",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);

				// METABOX TREINAMENTOS
				$metaboxes[] = array(
					'id'			=> 'metaboxTreinamentos',
					'title'			=> 'Detalhes do treinameto',
					'pages' 		=> array('treinamentos'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Local | Data do treinamento: ',
							'id'    => "{$prefix}treinamento_local",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Link saiba mais: ',
							'id'    => "{$prefix}treinamento_link",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxEquipe',
					'title'			=> 'Detalhes do integrante',
					'pages' 		=> array('equipe'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Função: ',
							'id'    => "{$prefix}equipe_funcao",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);
				// METABOX COMO SERVIÇOS
				$metaboxes[] = array(
					'id'			=> 'metaboxSevicos',
					'title'			=> 'Detalhes do serviço',
					'pages' 		=> array('servico'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Link saiba mais ',
							'id'    => "{$prefix}servico_saibaMais",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);
				// METABOX COMO SERVIÇOS
				$metaboxes[] = array(
					'id'			=> 'metaboxDepoimentos',
					'title'			=> 'Detalhes do depoimento',
					'pages' 		=> array('depoimento'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Autor ',
							'id'    => "{$prefix}depoimento_autor",
							'desc'  => '',
							'type'  => 'text',
							'placeholder' => 'Ruy Benson  | Lorem ipsum dolor',
							'desc' => 'Ruy Benson  | Lorem ipsum dolor',
						), 
					),
				);
				// METABOX PROUTOS
				$metaboxes[] = array(
					'id'			=> 'metaboxProdutos',
					'title'			=> 'Detalhes do produto',
					'pages' 		=> array('produto'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Subtítulo: ',
							'id'    => "{$prefix}produto_subtitulo",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'URL Video: ',
							'id'    => "{$prefix}url_video",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Breve descritivo: ',
							'id'    => "{$prefix}produto_breveDescritivo",
							'desc'  => '',
							'type'  => 'textarea',
						), 
						array(
							'name'  => 'Url do arquivo descritivo Técinico: ',
							'id'    => "{$prefix}produto_arquivo",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Galeria de fotos do produto: ',
							'id'    => "{$prefix}produto_galeria",
							'desc'  => '',
							'type'  => 'image_advanced',
						),
						array(
							'name'  => 'Características: ',
							'id'    => "{$prefix}produto_caracteristica",
							'desc'  => '',
							'type'  => 'wysiwyg',
						), 
						array(
							'name'  => 'Especificações: ',
							'id'    => "{$prefix}produto_especificacoes",
							'desc'  => '',
							'type'  => 'wysiwyg',
						), 
					),
				);

				//METABOX PÁGINA INICAL
				$metaboxes[] = array(
					'id'			=> 'metaboxPaginaInicial',
					'title'			=> 'Detalhes da página inicial - Entre na página correspondente ao campo',
					'pages' 		=> array('page'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						
						array(
							'name'  => 'Ícone Assistência: ',
							'id'    => "{$prefix}inicial_areaAssistencia_icone",
							'desc'  => '',
							'type'  => 'image_advanced',
							'max_file_uploads' => 1	
						),
						array(
							'name'  => 'Foto ilustrativa Assistência: ',
							'id'    => "{$prefix}inicial_areaAssistencia_Foto",
							'desc'  => '',
							'type'  => 'image_advanced',
							'max_file_uploads' => 1	
						),
						array(
							'name'  => 'Título Assistência técnica: ',
							'id'    => "{$prefix}inicial_areaAssistencia_titulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Texto Assistência técnica: ',
							'id'    => "{$prefix}inicial_areaAssistencia_texto",
							'desc'  => '',
							'type'  => 'wysiwyg',
						), 
						array(
							'name'  => 'Título Serviços: ',
							'id'    => "{$prefix}inicial_areaServicos_titulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Subitítulo Serviços: ',
							'id'    => "{$prefix}inicial_areaServicos_Subititulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Título  Produtos: ',
							'id'    => "{$prefix}inicial_areaProdutos_titulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Subitítulo Produtos: ',
							'id'    => "{$prefix}inicial_areaProdutos_Subititulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Título  Noticias: ',
							'id'    => "{$prefix}inicial_areaNoticias_titulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Subitítulo Noticias: ',
							'id'    => "{$prefix}inicial_areaNoticias_Subititulo",
							'desc'  => '',
							'type'  => 'text',
						),
					),
				);

				//METABOX PÁGINA EMPRESA
				$metaboxes[] = array(
					'id'			=> 'metaboxaEmpresa',
					'title'			=> 'Detalhes da página a empresa - Entre na página correspondente ao campo',
					'pages' 		=> array('page'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						
						array(
							'name'  => 'Ícone do texto: ',
							'id'    => "{$prefix}empresa_icone",
							'desc'  => '',
							'type'  => 'image_advanced',
							'max_file_uploads' => 1	
						),

						array(
							'name'  => 'Texto Estrutura: ',
							'id'    => "{$prefix}empresa_textoEstrutura",
							'desc'  => '',
							'type'  => 'wysiwyg',
						),
						array(
							'name'  => 'Galeria de imagens: ',
							'id'    => "{$prefix}empresa_galeria_imagens",
							'desc'  => '',
							'type'  => 'image_advanced',
						),
						array(
							'name'  => 'Título especialista: ',
							'id'    => "{$prefix}empresa_especialista_titulo",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Subtítulo especialista: ',
							'id'    => "{$prefix}empresa_especialista_sub",
							'desc'  => '',
							'type'  => 'text',
						),
					

					),
					
				);

				//METABOX PÁGINA ASSISTÊNCIA TÉCNICA
				$metaboxes[] = array(
					'id'			=> 'metaboxAssistencia',
					'title'			=> 'Detalhes da página Assistência - Entre na página correspondente ao campo',
					'pages' 		=> array('page'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						
					
						array(
							'name'  => 'Galeria de fotos: ',
							'id'    => "{$prefix}assitencia_galeria",
							'desc'  => '',
							'type'  => 'image_advanced',
						),
						array(
							'name'  => 'Tipo de assistência lado esquerdo: ',
							'id'    => "{$prefix}tipo_assistencia_esquerdo",
							'desc'  => '',
							'type'  => 'text',
							'clone'  => true,
						),
						array(
							'name'  => 'Tipo de assistência lado direito: ',
							'id'    => "{$prefix}tipo_assistencia_direito",
							'desc'  => '',
							'type'  => 'text',
							'clone'  => true,
						),

					),
					
				);

				//METABOX PÁGINA ASSISTÊNCIA TÉCNICA
				$metaboxes[] = array(
					'id'			=> 'metaboxTreinamentos',
					'title'			=> 'Detalhes da página treinamentos - Entre na página correspondente ao campo',
					'pages' 		=> array('page'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
					
						array(
							'name'  => 'Título do texto: ',
							'id'    => "{$prefix}treinamentos_titulo",
							'desc'  => '',
							'type'  => 'text',
						),

						array(
							'name'  => 'Título área cursos: ',
							'id'    => "{$prefix}treinamentos_areaCursos",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Subtítulo área cursos: ',
							'id'    => "{$prefix}treinamentos_areaCursosSub",
							'desc'  => '',
							'type'  => 'text',
						),

					),
					
				);

				return $metaboxes;
			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaTeikei () {
			taxonomiaCategoriaProdutos();
		}

		function taxonomiaCategoriaProdutos() {

			$rotulosCategoriaProdutos = array(
												'name'              => 'Categorias de produto',
												'singular_name'     => 'Categorias de produtos',
												'search_items'      => 'Buscar categoria do produto',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do produto',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias produtos',
											);

			$argsCategoriaProdutos 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaProdutos,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-produtos' ),
											);

			register_taxonomy( 'categoriaProdutos', array( 'produto' ), $argsCategoriaProdutos);

		}
		
		function metaboxjs(){



			global $post;

			$template = get_post_meta($post->ID, '_wp_page_template', true);

			$template = explode('/', $template);

			$template = explode('.', $template[1]);

			$template = $template[0];



			if($template != ''){

				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);

			}

		}


  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'baseTeikei');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {

	    	baseTeikei();

	   		flush_rewrite_rules();
		}

		register_activation_hook( __FILE__, 'rewrite_flush' );