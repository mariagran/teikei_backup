<?php
/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @package Teikei
 */

get_header(); ?>
<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">
	
	<!-- CARROSSSEL DESTAQUE -->
	<section class="carrosselDestaque sessao">
		<h6 class="hidden">Carrossel destaque</h6>

		<button id="btnCarrosselDestaqueLeft" class="hiddenElemento-sm "><i class="fa hvr-grow fa-angle-left" aria-hidden="true"></i></button>
		<button id="btnCarrosselDestaqueRight" class="hiddenElemento-sm "><i class="fa hvr-grow fa-angle-right" aria-hidden="true"></i></button>

		<!-- CARROSSEL DE DESTAQUE -->
		<div id="carrosselDestaque" class="owl-Carousel">
	
			<?php 
				//LOOP DE POST DESTAQUES
				$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
				while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
				$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaque = $fotoDestaque[0];
				$destaque_link = rwmb_meta('Teikei_destaque_link');
				
			 ?>
			<!-- ITEM -->
			<figure class="item" style="background: url(<?php echo $fotoDestaque ?>);">
				
				<div class="container hiddenElemento-sm">
					<div class="texto">
						<p><?php echo get_the_content() ?></p>
						
						<?php if ($destaque_link):?>
						<a href="<?php echo $destaque_link  ?>" target="_blank">Saiba mais <i class="fa fa-angle-right" aria-hidden="true"></i></a>
					<?php endif; ?>
					</div>
				</div>

			</figure>
			<?php endwhile; wp_reset_query(); ?>
			
		</div>

	</section>

	<!-- SOBRE A EMPRESA -->
	<section class="areaSobre">
		<h6 class="hidden">A Empresa</h6>
		<?php echo the_content() ?>
	</section>

	<!-- ÁREA LOGOS PARCEIROS -->
	<section class="areaParceiros">
		<h6 class="hidden">Área logo parceiros</h6>
		<!-- CARROSSEL DE PARCEIROS -->

		<div class="container">
			<div id="carrosselLogos" class="owl-Carousel">
			<?php 
				//LOOP DE POST PARCEIROS
				$parceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $parceiros->have_posts() ) : $parceiros->the_post();
				$fotoParceiros = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoParceiros = $fotoParceiros[0];
			 ?>
				<!-- ITEM -->
				<figure class="item">
					
					<!-- LOGO PARCEIRO-->
					<img src="<?php echo $fotoParceiros ?> " alt=" <?php get_the_title() ?> " class="hvr-push">
					
				</figure>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
	</section>
	
	<!-- ÁREA ASSISTÊNCIA  TÉCNICA -->
	<section class="areaAssistenciaTecnica container">
		<h6 class="hidden"><?php echo $textoAssistencia = rwmb_meta('Teikei_inicial_areaServicos_titulo'); ?></h6>
		
		
		<div class="areaConteudo">
			<article>
				<?php 
				$inicial_areaAssistencia_icone = rwmb_meta('Teikei_inicial_areaAssistencia_icone');
				foreach ($inicial_areaAssistencia_icone as $inicial_areaAssistencia_icone):
					$inicial_areaAssistencia_icone = $inicial_areaAssistencia_icone['full_url'];
					?>
					<img src=" <?php echo $inicial_areaAssistencia_icone ?> " alt="<?php echo $textoAssistencia = rwmb_meta('Teikei_inicial_inicial_areaServicos_titulo'); ?>" class="hiddenElemento-sm">
				<?php endforeach; ?>
				<h3><?php echo $textoAssistencia = rwmb_meta('Teikei_inicial_inicial_areaServicos_titulo'); ?></h3>
				<?php echo $textoAssistencia = rwmb_meta('Teikei_inicial_areaAssistencia_texto'); ?>
				<a href="<?php echo home_url('/assistencia-tecnica/'); ?>">Saiba mais</a>
			</article>
		</div>
	
			<?php 
				$inicial_areaAssistencia_Foto = rwmb_meta('Teikei_inicial_areaAssistencia_Foto');
				foreach ($inicial_areaAssistencia_Foto as $inicial_areaAssistencia_Foto):
					$inicial_areaAssistencia_Foto = $inicial_areaAssistencia_Foto['full_url'];
				?>
			<img src=" <?php echo $inicial_areaAssistencia_Foto ?> " alt="<?php echo $textoAssistencia = rwmb_meta('Teikei_inicial_inicial_areaServicos_titulo'); ?>" class="hiddenElemento-sm">
			<?php endforeach; ?>
	</section>

	<!-- ÁREA DE INFORMAÇÕES -->
	<section class="areaInfos">
		<h6><?php echo $inicial_areaServicos_titulo = rwmb_meta('Teikei_inicial_areaServicos_titulo'); ?></h6>
		<p><?php echo $inicial_areaServicos_Subititulo = rwmb_meta('Teikei_inicial_areaServicos_Subititulo'); ?></p>

		<!-- SERVIÇOS -->
		<div class="container correcaoContainer">
			<ul>
			<?php 
				//LOOP DE POST SERVIÇOS
				$servios = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $servios->have_posts() ) : $servios->the_post();
					$fotoServicos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoServicos = $fotoServicos[0];
					$servico_saibaMais = rwmb_meta('Teikei_servico_saibaMais');

			?>
				<li>
					<img src="<?php echo $fotoServicos ?>" alt="<?php echo get_the_title() ?>">
					<h3><?php echo get_the_title() ?></h3>
					<p><?php echo get_the_content()?></p>
					<?php if ($servico_saibaMais): ?>
					<a href="<?php echo $servico_saibaMais ?>">Saiba Mais</a>
					<?php endif; ?>
				</li>
			<?php endwhile; wp_reset_query(); ?>
			</ul>
		</div>
	</section>

	<section class="areaDepoimentos">
		<div class="container correcaoContainer">
			<div class="row">

				<div class="col-sm-6">
					<!-- CARROSSEL DE DESTAQUE -->
					<div id="carrosselDepoimentosEsquerdo" class="owl-Carousel">
						<?php 
							//LOOP DE POST DEPOIMENTOS
							$depoimentos = new WP_Query( array( 'post_type' => 'depoimento',  'orderby' => 'rand', 'posts_per_page' => -1 ) );
							while ( $depoimentos->have_posts() ) : $depoimentos->the_post();
								$fotoDepoimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDepoimento = $fotoDepoimento[0];
								$depoimento_autor = rwmb_meta('Teikei_depoimento_autor');
								$depoimento_autor = explode("|", $depoimento_autor);

						?>	
						<div class="item">
							<div class="depoimento row">
								<div class="col-sm-3">
									<img src=" <?php echo $fotoDepoimento ?> " alt=" <?php echo get_the_title() ?> ">
								</div>
								<div class="col-sm-9">
									<p><?php echo get_the_content() ?></p>

									<h3><?php echo $depoimento_autor[0] ?> </h3> <strong>| <?php echo $depoimento_autor[1] ?></strong>
								</div>
							</div>
						</div>
						<?php endwhile; wp_reset_query(); ?>
					</div>
				</div>

				<div class="col-sm-6">
					<!-- CARROSSEL DE DESTAQUE -->
					<div id="carrosselDepoimentosDireito" class="owl-Carousel">
						<?php 
							//LOOP DE POST DEPOIMENTOS
							$depoimentos = new WP_Query( array( 'post_type' => 'depoimento',  'orderby' => 'rand', 'posts_per_page' => -1 ) );
							while ( $depoimentos->have_posts() ) : $depoimentos->the_post();
								$fotoDepoimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDepoimento = $fotoDepoimento[0];
								$depoimento_autor = rwmb_meta('Teikei_depoimento_autor');
								$depoimento_autor = explode("|", $depoimento_autor);

						?>	
						<div class="item">
							<div class="depoimento row">
								<div class="col-sm-3">
									<img src=" <?php echo $fotoDepoimento ?> " alt=" <?php echo get_the_title() ?> ">
								</div>
								<div class="col-sm-9">
									<p><?php echo get_the_content() ?></p>

									<h3><?php echo $depoimento_autor[0] ?> </h3> <strong>| <?php echo $depoimento_autor[1] ?></strong>
								</div>
							</div>
						</div>
						<?php endwhile; wp_reset_query(); ?>
					</div>
				</div>

			</div>
		</div>
	</section>

	<!-- ÁREA DE PRODUTOS -->
	<section class="areaProdutos">
		<h6><?php echo $depoimento_autor = rwmb_meta('Teikei_inicial_areaProdutos_titulo'); ?></h6>
		<p><?php echo $depoimento_autor = rwmb_meta('Teikei_inicial_areaProdutos_Subititulo'); ?></p>

		<!-- SERVIÇOS -->
		<div class="areaCaroosel">
			<button id="btnCarrosselProdutoLeft"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
			<button id="btnCarrosselProdutoRight"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
			<div class="container">
				<div id="carrosselProdutos" class="owl-Carousel">
					<?php 
						//LOOP DE POST PRODUTOS
						$produtos = new WP_Query( array( 'post_type' => 'produto',  'orderby' => 'asc', 'posts_per_page' => -1 ) );
						while ( $produtos->have_posts() ) : $produtos->the_post();
							$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPost = $fotoPost[0];

					?>	
					<a href="<?php echo get_permalink() ?>" class="item">
						<figure>
							<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
						</figure>
						<h2> <?php echo get_the_title() ?> </h2>
						<p><?php echo $produto_breveDescritivo = rwmb_meta('Teikei_produto_breveDescritivo'); ?></p>
						<span>Saiba mais</span>
					</a>
					<?php endwhile; wp_reset_query(); ?>

				</div>
			</div>
		</div>
	</section>
	
	<!-- ÚLTIMAS NOTÍCIAS -->
	<section class="areaNoticias">
		<h6><?php echo $depoimento_autor = rwmb_meta('Teikei_inicial_areaNoticias_titulo'); ?></h6>
		<p><?php echo $depoimento_autor = rwmb_meta('Teikei_inicial_areaNoticias_Subititulo'); ?></p>

		<div class="noticias">
			<div class="container correcaoContainer">
				<div class="row">
					
					<?php 
						//LOOP DE POST POST
						$posts = new WP_Query( array( 'post_type' => 'post',  'orderby' => 'asc', 'posts_per_page' => 3 ) );
						while ( $posts->have_posts() ) : $posts->the_post();
							$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPost = $fotoPost[0];
					?>	
					<div class="col-sm-4">
						<a href="<?php echo get_permalink() ?> " class="post">
							<figure class="imgIlustrativa" style="background: url(<?php echo $fotoPost ?>)"></figure>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php customExcerpt(350); ?></p>
							<span>Saiba mais</span>
						</a>
					</div>
					<?php endwhile; wp_reset_query(); ?>
				
				</div>
			</div>

				<div class="text-center">
					<a href="<?php echo home_url('/blog/'); ?>" class="linkBlog">Saiba mais <i class="fa fa-angle-right" aria-hidden="true"></i></a>
				</div>
		</div>
	
	</section>

	<div class="mapaGoogle">
		<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
			<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
			<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
		</a>
	</div>

	<!-- ÁREA FOMRULÁRIO -->
	<section class="areaFomrularioContato">
		<h6>Entre em contato!</h6>
		<p>Preencha seus dados abaixo e em seguida entraremos em contato.</p>

		<div class="container">
			<div class="form">
				<?php echo do_shortcode('[contact-form-7 id="5" title="Formulário de contato"]'); ?>
			</div>
		</div>

	</section>
</div>

<?php get_footer(); ?>