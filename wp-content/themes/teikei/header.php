<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Teikei
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php
		echo  $configuracao['header_scripts'] ;
		wp_head(); 
	?>
</head>

<body <?php body_class(); ?>>
	<?php
		echo  $configuracao['header_body'] ;
	
	?>
	<!-- TOPO -->
	<header class="topo">
		<div class="container correcaoContainer">
			<div class="row">

				<div class="col-sm-3">
					<!-- LOGO -->
					<div class="logo">
						<a href="<?php echo home_url('/'); ?>">
							<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?> ">
						</a>
					</div>
				</div>					

				<div class="col-sm-9">

					<!-- ÁREA PARA CONTATO -->
					<div class="contatos hiddenElemento-sm">

						<div class="contatosTelefone">
							<a href="tel:<?php echo $configuracao['opt_telefone'] ?>"> <?php echo $configuracao['opt_telefone'] ?></a>
							<a href="malito:<?php echo $configuracao['opt_Email'] ?>"><?php echo $configuracao['opt_Email'] ?></a>
						</div>

						<div class="redesSociais">
							<a href="<?php echo $configuracao['opt_face'] ?>" target="_blank">
								<i class="fa  hvr-pop fa-facebook" aria-hidden="true"></i>
							</a>
							<a href="<?php echo $configuracao['opt_linkedin'] ?>" target="_blank">
								<i class="fa hvr-pop  fa-instagram" aria-hidden="true"></i>
							</a>
						</div>

					</div>

					<div class="navbar" role="navigation">	

						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<?php 
									$menu = array(
										'theme_location'  => '',
										'menu'            => 'Menu Principal',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $menu );
								?>
							</nav>						
						</div>			

					</div>

				</div>					
			</div>
		</div>
	</header>