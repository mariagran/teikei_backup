<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Teikei
 */
global $configuracao;
echo  $configuracao['header_rodape'] ;
?>

<!-- RODAPÉ -->
	<footer class="rodape">
		<div class="container correcaoContainer">
			<div class="row">
				
				<div class="col-sm-3 col-sm-12 col-xs-12">
					<div class="conteudo">
						<img src="<?php echo $configuracao['opt_logo_branca']['url'] ?> " alt="<?php echo get_bloginfo() ?>">
						<p class="hiddenColuna"><?php  //echo $configuracao['opt_info_rodape'] ?></p>

						<a href="<?php echo $configuracao['opt_face'] ?>" target="_blank" class="hiddenColuna hvr-pop">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a href="<?php echo $configuracao['opt_linkedin'] ?>"" target="_blank" class="hiddenColuna hvr-pop">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</a>
					</div>
				</div>

				<div class="col-sm-3 hiddenColuna">
					<div class="mapaSite">
						<span>Mapa do site</span>

						<?php 
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu Rodape',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
								);
							wp_nav_menu( $menu );
						?>
					</div>
				</div>

				<div class="col-sm-3 col-sm-12 col-xs-12">
					<div class="mapaSite">
						<span>Contato</span>

						<a href="tel:<?php echo $configuracao['opt_telefone'] ?>" class="linkcontato rodapePhone"><?php echo $configuracao['opt_telefone'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_telefone2'] ?>" class="linkcontato rodapeWhats"><?php echo $configuracao['opt_telefone2'] ?></a>
						<a href="malito:<?php echo $configuracao['opt_Email'] ?>" class="linkcontato rodapeEmail"><?php echo $configuracao['opt_Email'] ?></a>
						<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank" class="linkcontato rodapePin"><?php echo $configuracao['opt_endereco'] ?></a>
					</div>
				</div>

				<div class="col-sm-12 conteudoMobile">
					<div class="conteudo ">
						<a href="<?php echo $configuracao['opt_face'] ?>" target="_blank" class="hiddenColuna hvr-pop">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a href="<?php echo $configuracao['opt_linkedin'] ?>" target="_blank" class="hiddenColuna hvr-pop">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</a>
					</div>
				</div>

				<div class="col-sm-3 hiddenColuna">
					<div class="mapaSite">
						<span>Certificações</span>
						<img src="<?php echo $configuracao['opt_certificacao']['url'] ?>" alt="Celo de certificação">
					</div>
				</div>
				
			</div>
		</div>
		
	</footer>

	<!-- COPYRIGHT -->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<p><i class="fa fa-copyright" aria-hidden="true"></i> <?php echo $configuracao['opt_Copyryght'] ?></p>
				</div>
				<div class="col-md-6 hiddenColuna text-right ">
					<p>Desenvolvido por <a href="http://www.handgran.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/handgran.png" alt=""></a></p>
				</div>
			</div>
		</div>
	</div>

	</body>

<?php wp_footer(); ?>

</body>
</html>
