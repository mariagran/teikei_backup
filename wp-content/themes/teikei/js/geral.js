
$(function(){

	$(".navbar-collapse ul").addClass('nav navbar-nav');
	$("#collapse > div").removeClass('nav navbar-nav');
	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {

		//CARROSSEL DE DESTAQUE
		$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,		    
		});
		var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
		$('#btnCarrosselDestaqueLeft').click(function(){ carrossel_destaque.prev(); });
		$('#btnCarrosselDestaqueRight').click(function(){ carrossel_destaque.next(); });

		//CARROSSEL DE LOGOS
		$("#carrosselLogos").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,	
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },
			           
			            991:{
			                items:3
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:4
			            },
			            			            
			        }		   	    
		});

		//CARROSSEL DE DEPOIMENTOS
		$("#carrosselDepoimentosEsquerdo").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,		    
		});

		//CARROSSEL DE DEPOIMENTOS
		$("#carrosselDepoimentosDireito").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,		    
		});

		//CARROSSEL DE LOGOS
		$("#carrosselProdutos").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },
			           
			            991:{
			                items:2
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:4
			            },
			            			            
			        }		   		    
		});
		var carrossel_produto = $("#carrosselProdutos").data('owlCarousel');
		$('#btnCarrosselProdutoLeft').click(function(){ carrossel_produto.prev(); });
		$('#btnCarrosselProdutoRight').click(function(){ carrossel_produto.next(); });
	});

	/*****************************************
		SCRIPTS PÁGINA A EMPRESA
	*******************************************/
	$(document).ready(function() {
		//CARROSSEL DE ESTRUTURA
		$("#carrosselEstrutura").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,		    
		});
		$("#carrosselAssistencia").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,		    
		});
		//CARROSSEL DE LOGOS
		$("#carrosselLogosAssistencia").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:3
			            },
			           
			            991:{
			                items:3
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:4
			            },
			            			            
			        }		   		    
		});
		$("#carrosselProduto").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,		    
		});
	});
});

$('.scrollTop').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			$('html,body').animate({
				scrollTop: target.offset().top
			}, 1000);
			return false;
		}
	}
	
});
